# General stuff
picom &
nitrogen --restore &

# xsetroot for dwm
while true; do
BATT=$(acpi -b | cut -d ' ' -f 4 | tr -d ',')
BATSTAT=$(acpi -b | cut -d ' ' -f 3 | tr -d ',')
DATE=$(/bin/date +"%a  %b %d  %H:%M")
MEMI=$(free -m | grep '^Mem' | awk '{print $3/$2*100}')
MEMF=$(printf '%.*f\n' 1 $MEMI)
VOL=$(vol=$(pulsemixer --get-volume | awk '{ print $1 }')
	mute=$(pulsemixer --get-mute)	
	if [[ "$mute" -eq "1" ]]; then
		icon= #mute icon
	else
		if [[ "$vol" -le "30" ]]; then
			icon= #low volume icon
		elif [[ "$vol" -gt "30" && "$vol" -le "55" ]]; then
			icon= #medium volume icon
		elif [[ "$vol" -gt "55" ]]; then
			icon= #high volume icon
		fi
	fi

	echo "$icon  $vol%")
ROOT=" [  $MEMF%] [$VOL] [ $BATT - $BATSTAT] $DATE"
xsetroot -name "$ROOT"
done &
